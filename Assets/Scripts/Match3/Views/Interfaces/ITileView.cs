using System;
using Match3.Scriptables;
using UnityEngine;

namespace Match3.Views.Interfaces
{
	public interface ITileView : IView
	{
		#region PublicFields

		Vector2Int Position { set; }
		float Size { set; }
		bool IsSelected { set; }

		#endregion

		#region EventsAndDelegates

		event Action Selected;

		#endregion

		#region PublicMethods

		void UpdateTileConfig(TilesConfig.Tile tileConfig);

		#endregion
	}
}
