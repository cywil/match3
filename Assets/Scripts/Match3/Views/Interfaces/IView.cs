using UnityEngine;

namespace Match3.Views.Interfaces
{
	public interface IView
	{
		#region PublicFields

		Transform ViewTransform { get; }

		#endregion

		#region PublicMethods

		void Show();
		void Hide();
		void HideImmediate();
		void ResetView();
		void Destroy();

		#endregion
	}
}
