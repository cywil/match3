using Match3.Pooling;
using Match3.Views.Interfaces;
using UnityEngine;

namespace Match3.Views.Abstracts
{
	public abstract class AbstractView : MonoBehaviour, IView, IPoolable
	{
		#region PublicFields

		public Transform ViewTransform
		{
			get
			{
				if (m_cachedTransform == null)
				{
					m_cachedTransform = transform;
				}

				return m_cachedTransform;
			}
		}

		public bool InPool { get; set; }

		#endregion

		#region PrivateFields

		private Transform m_cachedTransform;

		#endregion

		#region InterfaceImplementations

		public virtual void OnSpawn()
		{
			Show();
		}

		public virtual void Despawn()
		{
			InPool = true;
			HideImmediate();
		}

		public virtual void Show()
		{
			gameObject.SetActive(true);
		}

		public virtual void Hide()
		{
			gameObject.SetActive(false);
		}

		public void HideImmediate()
		{
			gameObject.SetActive(false);
		}

		public virtual void Destroy()
		{
			Destroy(gameObject);
		}

		public virtual void ResetView()
		{ }

		#endregion
	}
}
