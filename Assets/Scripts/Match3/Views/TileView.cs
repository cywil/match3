using System;
using Match3.Scriptables;
using Match3.Views.Abstracts;
using Match3.Views.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Match3.Views
{
	public sealed class TileView : AbstractView, ITileView, IPointerClickHandler
	{
		#region Constants

		private const float INIT_HEIGHT_OFFSET = 5f;
		private const float MOVE_ANIMATION_THRESHOLD = 0.00001f;

		#endregion

		#region PublicFields

		public Vector2Int Position
		{
			set
			{
				Vector3 targetPosition = new Vector3(value.x, value.y, 0) * m_size;

				if (!m_targetPosition.HasValue)
				{
					transform.localPosition = Vector3.up * INIT_HEIGHT_OFFSET + targetPosition;
				}

				gameObject.name = value.ToString();
				m_targetPosition = targetPosition;
			}
		}

		public float Size
		{
			set
			{
				m_size = value;
				ViewTransform.localScale = Vector3.one * value;
			}
		}

		public bool IsSelected
		{
			set => m_sprite.transform.localScale = Vector3.one * (value ? 1 : .8f);
		}

		#endregion

		#region EventsAndDelegates

		public event Action Selected;

		#endregion

		#region SerializeFields

		[SerializeField] private float m_moveSpeed;
		[SerializeField] private SpriteRenderer m_sprite;

		#endregion

		#region PrivateFields

		private Vector3? m_targetPosition;
		private float m_size;

		#endregion

		#region UnityMethods

		private void Update()
		{
			if (!m_targetPosition.HasValue)
			{
				return;
			}

			if ((ViewTransform.localPosition - m_targetPosition.Value).sqrMagnitude <= MOVE_ANIMATION_THRESHOLD)
			{
				return;
			}

			if (Mathf.Approximately(m_moveSpeed, 0))
			{
				ViewTransform.localPosition = m_targetPosition.Value;
				return;
			}

			ViewTransform.localPosition = Vector3.Lerp(transform.localPosition, m_targetPosition.Value, Time.deltaTime * m_moveSpeed);
		}

		#endregion

		#region InterfaceImplementations

		public void OnPointerClick(PointerEventData eventData)
		{
			Selected?.Invoke();
		}

		public void UpdateTileConfig(TilesConfig.Tile tileConfig)
		{
			m_sprite.color = tileConfig.Color;
		}

		public override void Destroy()
		{
			Despawn();
		}

		#endregion

		#region PublicMethods

		public override void Despawn()
		{
			base.Despawn();

			gameObject.name = "Tile";
			m_targetPosition = null;
			Selected = null;
		}

		#endregion
	}
}
