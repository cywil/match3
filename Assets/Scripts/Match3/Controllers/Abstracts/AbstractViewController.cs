﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Match3.Controllers.Interfaces;
using Match3.Models.Interfaces;
using Match3.Views.Interfaces;
using Utils.Extensions;

namespace Match3.Controllers.Abstracts
{
	public abstract class AbstractViewController<TModel, TView> : IViewController
		where TModel : class, IModel
		where TView : class, IView
	{
		#region ProtectedFields

		protected TModel m_model;
		protected TView m_view;

		#endregion

		#region PrivateFields

		private readonly Dictionary<string, Action> m_modelChanged;

		#endregion

		#region Constructors

		protected AbstractViewController(TModel model, TView view, bool hideViewImmediate = false)
		{
			m_model = model.CheckNotNull(nameof(model));
			m_view = view.CheckNotNull(nameof(view));

			m_modelChanged = new Dictionary<string, Action>();
			m_model.PropertyChanged += OnModelPropertyChanged;

			if (hideViewImmediate)
			{
				view.HideImmediate();
			}
		}

		#endregion

		#region InterfaceImplementations

		public virtual void Dispose()
		{
			if (m_view != null && !m_view.Equals(null))
			{
				m_view.Destroy();
			}

			m_view = default;

			if (m_model != null)
			{
				m_model.PropertyChanged -= OnModelPropertyChanged;
				m_model = null;
			}
		}

		public virtual void Show()
		{
			m_view.Show();
		}

		public virtual void Hide()
		{
			m_view.Hide();
		}

		#endregion

		#region ProtectedMethods

		protected void RegisterModelChangedCallback(string propertyName, Action action)
		{
			m_modelChanged[propertyName] = action;
		}

		protected void UpdateAllModelProperties()
		{
			foreach (KeyValuePair<string, Action> keyValuePair in m_modelChanged)
			{
				keyValuePair.Value?.Invoke();
			}
		}

		protected void UpdateProperty(string propertyName)
		{
			if (m_modelChanged.TryGetValue(propertyName, out Action updateAction))
			{
				updateAction?.Invoke();
			}
		}

		#endregion

		#region PrivateMethods

		private void OnModelPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			UpdateProperty(e.PropertyName);
		}

		#endregion
	}
}
