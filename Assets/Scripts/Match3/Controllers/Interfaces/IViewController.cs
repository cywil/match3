﻿using System;

namespace Match3.Controllers.Interfaces
{
	public interface IViewController : IDisposable
	{
		#region PublicMethods

		void Show();
		void Hide();

		#endregion
	}
}
