using System;
using System.Collections.Generic;
using Match3.Controllers.Abstracts;
using Match3.Models;
using Match3.Scriptables;
using Match3.Views.Interfaces;
using UnityEngine;

namespace Match3.Controllers
{
	public sealed class TileController : AbstractViewController<TileModel, ITileView>
	{
		#region PublicFields

		public Vector2Int Position
		{
			get => m_model.Position;
			set => m_model.Position = value;
		}

		public bool IsSelected
		{
			set => m_model.IsSelected = value;
		}

		public bool Matched
		{
			get => m_model.Matched;
			set => m_model.Matched = value;
		}

		public TilesConfig.Tile TileConfig => m_model.TileConfig;

		#endregion

		#region EventsAndDelegates

		public event Action<TileController> Selected;
		private Dictionary<string, Action> m_tileModelChangedActions;

		#endregion

		#region Constructors

		public TileController(TileModel model, ITileView view, bool hideViewImmediate = false) : base(model, view, hideViewImmediate)
		{
			view.Selected += OnSelected;
			RegisterModelChanged();
		}

		#endregion

		#region PublicMethods

		public override void Dispose()
		{
			base.Dispose();

			if (m_view != null)
			{
				m_view.Selected -= OnSelected;
			}

			Selected = null;
		}

		public void SetViewParent(Transform parent)
		{
			m_view.ViewTransform.SetParent(parent, false);
		}

		#endregion

		#region PrivateMethods

		private void OnSelected()
		{
			Selected?.Invoke(this);
		}

		private void RegisterModelChanged()
		{
			RegisterModelChangedCallback(nameof(TileModel.TileConfig), () => m_view.UpdateTileConfig(m_model.TileConfig));
			RegisterModelChangedCallback(nameof(TileModel.Size), () => m_view.Size = m_model.Size);
			RegisterModelChangedCallback(nameof(TileModel.Position), () => m_view.Position = m_model.Position);
			RegisterModelChangedCallback(nameof(TileModel.IsSelected), () => m_view.IsSelected = m_model.IsSelected);

			UpdateAllModelProperties();
		}

		#endregion
	}
}
