using System.Collections.Generic;
using Match3.Controllers.Abstracts;
using Match3.Level;
using Match3.Level.Interfaces;
using Match3.Models;
using Match3.Pooling;
using Match3.Scriptables;
using Match3.Views;
using Match3.Views.Interfaces;
using UnityEngine;
using Utils.Extensions;

namespace Match3.Controllers
{
	public sealed class LevelController : AbstractViewController<LevelModel, ILevelView>
	{
		#region PrivateFields

		private LevelConfig m_levelConfig;
		private ILevelGenerator m_levelGenerator;
		private ILevelResolver m_levelResolver;
		private ViewPool m_viewPool;

		#endregion

		#region Constructors

		public LevelController(LevelModel model, ILevelView view, LevelConfig levelConfig, ViewPool viewPool) : base(model, view)
		{
			m_levelConfig = levelConfig.CheckNotNull(nameof(levelConfig));
			m_viewPool = viewPool.CheckNotNull(nameof(viewPool));

			m_levelGenerator = new LevelGenerator(m_levelConfig, TileViewFactory);
			m_model.Grid = m_levelGenerator.GenerateLevelGrid();
			m_levelResolver = new LevelResolver(m_model.Grid, m_levelGenerator);

			RegisterModelChanged();
			SetupLevelView();
		}

		#endregion

		#region PublicMethods

		public override void Dispose()
		{
			base.Dispose();

			if (m_model != null)
			{
				foreach (TileController tileController in m_model.Grid)
				{
					tileController?.Dispose();
				}

				m_model.Grid.ClearNodes();
			}
		}

		#endregion

		#region PrivateMethods

		private ITileView TileViewFactory()
		{
			return m_viewPool.Spawn<TileView>();
		}

		private void RegisterModelChanged()
		{
			RegisterModelChangedCallback(nameof(LevelModel.FirstSelectedTile), () => SetTileSelection(m_model.FirstSelectedTile, true));
			RegisterModelChangedCallback(nameof(LevelModel.SecondSelectedTile), () => SetTileSelection(m_model.SecondSelectedTile, true));
		}

		private void SetTileSelection(TileController tile, bool isSelected)
		{
			if (tile != null)
			{
				tile.IsSelected = isSelected;
			}
		}

		private void SetupLevelView()
		{
			foreach (TileController tile in m_model.Grid)
			{
				InitializeTile(tile);
			}

			int width = m_model.Grid.Width;
			int height = m_model.Grid.Height;
			float tileSize = m_levelConfig.TileSize;

			m_view.ViewTransform.position = new Vector3(-width / 2f * tileSize + tileSize / 2f, -height * tileSize / 2f + tileSize / 2f, 0);
		}

		private void InitializeTile(TileController tile)
		{
			tile.SetViewParent(m_view.ViewTransform);
			tile.Selected += OnTileSelected;
		}

		private void OnTileSelected(TileController tile)
		{
			if (m_model.FirstSelectedTile == null && m_model.SecondSelectedTile == null)
			{
				m_model.FirstSelectedTile = m_model.Grid.GetNode(tile.Position);
				return;
			}

			if (m_model.FirstSelectedTile != null && m_model.SecondSelectedTile == null)
			{
				if (m_model.Grid.AreNeighbours(m_model.FirstSelectedTile.Position, tile.Position))
				{
					m_model.SecondSelectedTile = tile;
				}
				else
				{
					SetTileSelection(m_model.FirstSelectedTile, false);
					m_model.FirstSelectedTile = tile;
				}
			}

			SwapTiles();
		}

		private void SwapTiles()
		{
			if (m_model.FirstSelectedTile == null || m_model.SecondSelectedTile == null)
			{
				return;
			}

			Vector2Int firstTilePosition = m_model.FirstSelectedTile.Position;
			Vector2Int secondTilePosition = m_model.SecondSelectedTile.Position;

			m_model.Grid.SetNode(m_model.FirstSelectedTile.Position, m_model.SecondSelectedTile);
			m_model.Grid.SetNode(m_model.SecondSelectedTile.Position, m_model.FirstSelectedTile);

			m_model.FirstSelectedTile.Position = secondTilePosition;
			m_model.SecondSelectedTile.Position = firstTilePosition;

			SetTileSelection(m_model.FirstSelectedTile, false);
			SetTileSelection(m_model.SecondSelectedTile, false);

			m_model.FirstSelectedTile = default;
			m_model.SecondSelectedTile = default;

			List<TileController> newTiles = m_levelResolver.ResolveLevel();

			foreach (TileController tile in newTiles)
			{
				InitializeTile(tile);
			}
		}

		#endregion
	}
}
