using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Match3.Models.Interfaces;

namespace Match3.Models
{
	public abstract class AbstractModel : IModel
	{
		#region EventsAndDelegates

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region ProtectedMethods

		[NotifyPropertyChangedInvocator]
		protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
