using Match3.Scriptables;
using UnityEngine;

namespace Match3.Models
{
	public sealed class TileModel : AbstractModel
	{
		#region PublicFields

		public TilesConfig.Tile TileConfig
		{
			get => m_tileConfig;
			set
			{
				if (m_tileConfig.TileType == value.TileType)
				{
					return;
				}

				m_tileConfig = value;
				OnPropertyChanged();
			}
		}

		public Vector2Int Position
		{
			get => m_position;
			set
			{
				if (m_position == value)
				{
					return;
				}

				m_position = value;
				OnPropertyChanged();
			}
		}

		public float Size
		{
			get => m_size;
			set
			{
				m_size = value;
				OnPropertyChanged();
			}
		}

		public bool IsSelected
		{
			get => m_isSelected;
			set
			{
				if (m_isSelected == value)
				{
					return;
				}

				m_isSelected = value;
				OnPropertyChanged();
			}
		}

		public bool Matched
		{
			get => m_matched;
			set
			{
				if (m_matched == value)
				{
					return;
				}

				m_matched = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region PrivateFields

		private TilesConfig.Tile m_tileConfig;
		private Vector2Int m_position;
		private float m_size;
		private bool m_isSelected;
		private bool m_matched;

		#endregion

		#region Constructors

		public TileModel(TilesConfig.Tile tileConfig, Vector2Int position, float size)
		{
			m_tileConfig = tileConfig;
			m_position = position;
			m_size = size;
		}

		#endregion
	}
}
