using Match3.Controllers;
using Utils.Grid;

namespace Match3.Models
{
	public sealed class LevelModel : AbstractModel
	{
		#region PublicFields

		public Grid<TileController> Grid
		{
			get => m_grid;
			set
			{
				if (m_grid == value)
				{
					return;
				}

				m_grid = value;
				OnPropertyChanged();
			}
		}

		public TileController FirstSelectedTile
		{
			get => m_firstSelectedTile;
			set
			{
				if (m_firstSelectedTile == value)
				{
					return;
				}

				m_firstSelectedTile = value;
				OnPropertyChanged();
			}
		}

		public TileController SecondSelectedTile
		{
			get => m_secondSelectedTile;
			set
			{
				if (m_secondSelectedTile == value)
				{
					return;
				}

				m_secondSelectedTile = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region PrivateFields

		private Grid<TileController> m_grid;
		private TileController m_firstSelectedTile;
		private TileController m_secondSelectedTile;

		#endregion
	}
}
