using System.ComponentModel;

namespace Match3.Models.Interfaces
{
	public interface IModel : INotifyPropertyChanged
	{ }
}
