using System;

namespace Match3.Enums
{
	[Flags]
	public enum ETileType
	{
		Red = 1 << 0,
		Orange = 1 << 1,
		Yellow = 1 << 2,
		Green = 1 << 3,
		Blue = 1 << 4,
		Purple = 1 << 5
	}
}
