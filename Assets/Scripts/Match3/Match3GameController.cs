﻿using Match3.Controllers;
using Match3.Models;
using Match3.Pooling;
using Match3.Scriptables;
using Match3.Views;
using UnityEngine;

namespace Match3
{
	public sealed class Match3GameController : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private LevelConfig m_levelConfig;
		[SerializeField] private LevelView m_levelView;
		[SerializeField] private ViewPool m_viewPool;

		#endregion

		#region PrivateFields

		private LevelController m_levelController;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_viewPool.AddPoolEntry(m_levelConfig.TilesConfig.TileViewPrefab, m_levelConfig.Width * m_levelConfig.Height);
			m_levelController = new LevelController(new LevelModel(), m_levelView, m_levelConfig, m_viewPool);
		}

		private void OnDestroy()
		{
			m_levelController?.Dispose();

			if (m_viewPool != null)
			{
				m_viewPool.Dispose();
				Destroy(m_viewPool.gameObject);
			}
		}

		#endregion
	}
}
