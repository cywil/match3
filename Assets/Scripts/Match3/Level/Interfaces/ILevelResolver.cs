using System.Collections.Generic;
using Match3.Controllers;

namespace Match3.Level.Interfaces
{
	public interface ILevelResolver
	{
		#region PublicMethods

		List<TileController> ResolveLevel();

		#endregion
	}
}
