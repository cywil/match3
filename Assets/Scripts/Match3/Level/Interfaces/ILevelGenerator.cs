using System.Collections.Generic;
using Match3.Controllers;
using Match3.Enums;
using UnityEngine;
using Utils.Grid;

namespace Match3.Level.Interfaces
{
	public interface ILevelGenerator
	{
		#region PublicMethods

		Grid<TileController> GenerateLevelGrid();
		List<TileController> GenerateNewTiles();
		bool AllNeighborsAreType(ETileType compareTo, List<(TileController, Vector2Int)> neighbours);

		#endregion
	}
}
