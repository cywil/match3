using System;
using System.Collections.Generic;
using Match3.Controllers;
using Match3.Enums;
using Match3.Level.Interfaces;
using Match3.Models;
using Match3.Scriptables;
using Match3.Views.Interfaces;
using UnityEngine;
using Utils.Extensions;
using Utils.Grid;
using Random = System.Random;

namespace Match3.Level
{
	public sealed class LevelGenerator : ILevelGenerator
	{
		#region Constants

		private const int MATCH_LENGTH = 2;

		#endregion

		#region PrivateFields

		private LevelConfig m_levelConfig;
		private Random m_random;
		private List<TilesConfig.Tile> m_tilesConfig;
		private Grid<TileController> m_grid;
		private Func<ITileView> m_tileViewFactory;

		#endregion

		#region Constructors

		public LevelGenerator(LevelConfig levelConfig, Func<ITileView> tileViewFactory)
		{
			m_levelConfig = levelConfig.CheckNotNull(nameof(levelConfig));
			m_tileViewFactory = tileViewFactory.CheckNotNull(nameof(tileViewFactory));

			m_random = new Random(m_levelConfig.Seed);
			m_tilesConfig = m_levelConfig.GetTilesConfig();
		}

		#endregion

		#region InterfaceImplementations

		public Grid<TileController> GenerateLevelGrid()
		{
			m_grid = new Grid<TileController>(m_levelConfig.Width, m_levelConfig.Height, CreateTileController);
			return m_grid;
		}

		public List<TileController> GenerateNewTiles()
		{
			var newTiles = new List<TileController>();

			for (int i = 0; i < m_grid.Width; i++)
			{
				for (int j = 0; j < m_grid.Height; j++)
				{
					TileController tile = m_grid.GetNode(i, j);

					if (tile != null)
					{
						continue;
					}

					var position = new Vector2Int(i, j);
					tile = CreateTileController(position, m_grid);

					m_grid.SetNode(position, tile);
					newTiles.Add(tile);
				}
			}

			return newTiles;
		}

		public bool AllNeighborsAreType(ETileType compareTo, List<(TileController, Vector2Int)> neighbours)
		{
			if (neighbours.Count < MATCH_LENGTH)
			{
				return false;
			}

			foreach ((TileController tile, Vector2Int position) in neighbours)
			{
				if (tile == null)
				{
					continue;
				}

				if (tile.TileConfig.TileType != compareTo)
				{
					return false;
				}
			}

			return true;
		}

		#endregion

		#region PrivateMethods

		private TileController CreateTileController(Vector2Int position, Grid<TileController> grid)
		{
			TilesConfig.Tile tileConfig = DrawTileConfig(position, grid);
			return new TileController(new TileModel(tileConfig, position, m_levelConfig.TileSize), m_tileViewFactory?.Invoke());
		}

		private TilesConfig.Tile DrawTileConfig(Vector2Int position, Grid<TileController> grid)
		{
			int tileNumber = m_random.Next(m_tilesConfig.Count);
			TilesConfig.Tile tile = m_tilesConfig[tileNumber];

			if (AllNeighborsAreType(tile.TileType, grid.GetNeighboursInDirection(position, Vector2Int.left * MATCH_LENGTH)))
			{
				return DrawTileConfig(position, grid);
			}

			if (AllNeighborsAreType(tile.TileType, grid.GetNeighboursInDirection(position, Vector2Int.down * MATCH_LENGTH)))
			{
				return DrawTileConfig(position, grid);
			}

			return tile;
		}

		#endregion
	}
}
