using System.Collections.Generic;
using Match3.Controllers;
using Match3.Level.Interfaces;
using UnityEngine;
using Utils.Extensions;
using Utils.Grid;

namespace Match3.Level
{
	public sealed class LevelResolver : ILevelResolver
	{
		#region Constants

		private const int MATCH_LENGTH = 2;

		#endregion

		#region PrivateFields

		private readonly Grid<TileController> m_grid;
		private readonly ILevelGenerator m_levelGenerator;
		private readonly List<TileController> m_generatedTiles;

		#endregion

		#region Constructors

		public LevelResolver(Grid<TileController> grid, ILevelGenerator levelGenerator)
		{
			m_levelGenerator = levelGenerator.CheckNotNull(nameof(levelGenerator));
			m_grid = grid.CheckNotNull(nameof(levelGenerator));

			m_generatedTiles = new List<TileController>();
		}

		#endregion

		#region InterfaceImplementations

		public List<TileController> ResolveLevel()
		{
			m_generatedTiles.Clear();

			while (MatchLevel())
			{
				RemoveMatched();
				FallDown();
				m_generatedTiles.AddRange(m_levelGenerator.GenerateNewTiles());
			}

			return m_generatedTiles;
		}

		#endregion

		#region PrivateMethods

		private bool MatchLevel()
		{
			bool anyMatched = false;

			foreach (TileController tile in m_grid)
			{
				if (tile == null)
				{
					continue;
				}

				List<(TileController, Vector2Int)> leftNeighbours = m_grid.GetNeighboursInDirection(tile.Position, Vector2Int.left * MATCH_LENGTH);

				if (m_levelGenerator.AllNeighborsAreType(tile.TileConfig.TileType, leftNeighbours))
				{
					foreach ((TileController neighbourTile, Vector2Int position) in leftNeighbours)
					{
						neighbourTile.Matched = true;
					}

					tile.Matched = true;
					anyMatched = true;
				}

				List<(TileController, Vector2Int)> downNeighbours = m_grid.GetNeighboursInDirection(tile.Position, Vector2Int.down * MATCH_LENGTH);

				if (m_levelGenerator.AllNeighborsAreType(tile.TileConfig.TileType, downNeighbours))
				{
					foreach ((TileController neighbourTile, Vector2Int position) in downNeighbours)
					{
						neighbourTile.Matched = true;
					}

					tile.Matched = true;
					anyMatched = true;
				}
			}

			return anyMatched;
		}

		private void RemoveMatched()
		{
			foreach (TileController tile in m_grid)
			{
				if (tile == null)
				{
					continue;
				}

				if (tile.Matched)
				{
					m_grid.SetNode(tile.Position, null);

					tile.Dispose();
					m_generatedTiles.Remove(tile);
				}
			}
		}

		private void FallDown()
		{
			for (int i = 0; i < m_grid.Width; i++)
			{
				int emptyTileBelow = 0;

				for (int j = 0; j < m_grid.Height; j++)
				{
					TileController tile = m_grid.GetNode(i, j);

					if (tile == null)
					{
						emptyTileBelow++;
						continue;
					}

					m_grid.SetNode(tile.Position, null);
					tile.Position += Vector2Int.down * emptyTileBelow;
					m_grid.SetNode(tile.Position, tile);
				}
			}
		}

		#endregion
	}
}
