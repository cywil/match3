using UnityEngine;

namespace Match3.Pooling
{
	public interface IPool
	{
		#region PublicMethods

		T Spawn<T>()
			where T : MonoBehaviour, IPoolable;

		void Despawn(IPoolable obj);

		#endregion
	}
}
