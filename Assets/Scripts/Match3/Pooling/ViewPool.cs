using System;
using System.Collections.Generic;
using System.Linq;
using Match3.Views.Abstracts;
using UnityEngine;

namespace Match3.Pooling
{
	public class ViewPool : MonoBehaviour, IPool, IDisposable
	{
		#region SerializeFields

		[Range(1, 100)] [SerializeField] private int m_expandRange;
		[SerializeField] private List<PoolEntry> m_viewPool;
		[SerializeField] private bool m_debug;

		#endregion

		#region PrivateFields

		private Dictionary<Type, List<IPoolable>> m_pool;
		private bool m_initialized;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			Initialize();
		}

		#endregion

		#region InterfaceImplementations

		public void Dispose()
		{
			foreach (KeyValuePair<Type, List<IPoolable>> keyValuePair in m_pool)
			{
				foreach (IPoolable poolable in keyValuePair.Value)
				{
					Destroy(((AbstractView)poolable).gameObject);
				}
			}

			m_initialized = false;
		}

		public T Spawn<T>()
			where T : MonoBehaviour, IPoolable
		{
			Initialize();

			var pooledObject = GetPooledObject<T>();

			if (pooledObject == null)
			{
				Debug.LogWarning($"Can't spawn {typeof(T)}.");
				return null;
			}

			pooledObject.InPool = false;
			pooledObject.OnSpawn();

			return pooledObject;
		}

		public void Despawn(IPoolable obj)
		{
			obj.Despawn();
		}

		#endregion

		#region PublicMethods

		public void AddPoolEntry(AbstractView view, int count)
		{
			Initialize();

			var poolEntry = new PoolEntry { View = view, Count = count };

			m_viewPool.Add(poolEntry);
			CreatePool(poolEntry);
		}

		#endregion

		#region PrivateMethods

		private void LogDebug(string message = "")
		{
			if (!m_debug)
			{
				return;
			}

			Debug.Log(message);

			foreach (KeyValuePair<Type, List<IPoolable>> pool in m_pool)
			{
				Debug.Log($"Pool {pool.Key}: {pool.Value.Count}");
			}
		}

		private T GetPooledObject<T>()
			where T : MonoBehaviour, IPoolable
		{
			Type type = typeof(T);

			if (!m_pool.ContainsKey(type))
			{
				Debug.Log($"Pool for {type} doesn't created. Expand failed.");
				return null;
			}

			foreach (IPoolable pooledObject in m_pool[type])
			{
				if (pooledObject.InPool)
				{
					return pooledObject as T;
				}
			}

			return ExpandPool<T>(m_expandRange);
		}

		private void Initialize()
		{
			if (m_initialized)
			{
				return;
			}

			m_pool = new Dictionary<Type, List<IPoolable>>();

			foreach (PoolEntry viewPool in m_viewPool)
			{
				CreatePool(viewPool);
			}

			m_initialized = true;
		}

		private void CreatePool(PoolEntry poolEntry)
		{
			if (poolEntry.View == null)
			{
				return;
			}

			Type viewType = poolEntry.View.GetType();
			if (m_pool.ContainsKey(viewType))
			{
				Debug.Log($"Pool for {viewType} already created.");
				return;
			}

			var pooledObjects = new List<IPoolable>();

			for (int i = 0; i < poolEntry.Count; i++)
			{
				AbstractView instance = Instantiate(poolEntry.View, transform);
				instance.name = poolEntry.View.name;

				pooledObjects.Add(instance);
				instance.InPool = true;
				Despawn(instance);
			}

			m_pool[viewType] = pooledObjects;
		}

		private T ExpandPool<T>(int count)
			where T : MonoBehaviour, IPoolable
		{
			Type type = typeof(T);

			if (!m_pool.ContainsKey(type))
			{
				Debug.LogError($"Pool for {type} not created. Expand failed.");
				return null;
			}

			List<IPoolable> pooledObjects = m_pool[type];

			if (count == 0)
			{
				// TODO : pick the oldest object
				var recycledObject = pooledObjects[0] as T;
				recycledObject.Despawn();

				return recycledObject;
			}

			var viewPrefab = m_viewPool.FirstOrDefault(e => e.View.GetType() == typeof(T)).View as T;

			if (viewPrefab == null)
			{
				Debug.LogError($"Prefab {type} is null.");
				return null;
			}

			T instance = null;

			for (int i = 0; i < count; i++)
			{
				instance = Instantiate(viewPrefab, transform);
				instance.name = viewPrefab.name;

				pooledObjects.Add(instance);
				Despawn(instance);
			}

			LogDebug("Pool expanded.");

			return instance;
		}

		#endregion

		#region NestedTypes

		[Serializable]
		private struct PoolEntry
		{
			public AbstractView View;
			public int Count;
		}

		#endregion
	}
}
