namespace Match3.Pooling
{
	public interface IPoolable
	{
		#region PublicFields

		bool InPool { get; set; }

		#endregion

		#region PublicMethods

		void OnSpawn();
		void Despawn();

		#endregion
	}
}
