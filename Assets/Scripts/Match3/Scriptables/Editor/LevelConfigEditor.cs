using UnityEditor;
using UnityEngine;

namespace Match3.Scriptables.Editor
{
	[CustomEditor(typeof(LevelConfig))]
	public class LevelConfigEditor : UnityEditor.Editor
	{
		#region PrivateFields

		private LevelConfig m_levelConfig;

		#endregion

		#region PublicMethods

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			if (m_levelConfig == null)
			{
				m_levelConfig = (LevelConfig)target;
			}

			GUILayout.Space(20);

			if (GUILayout.Button("Generate seed"))
			{
				m_levelConfig.GenerateSeed();
			}
		}

		#endregion
	}
}
