using System;
using System.Collections.Generic;
using System.Linq;
using Match3.Enums;
using UnityEngine;
using Utils.Extensions;

namespace Match3.Scriptables
{
	[CreateAssetMenu(fileName = "LevelConfig", menuName = "Match3/LevelConfig", order = 1)]
	public sealed class LevelConfig : ScriptableObject
	{
		#region Constants

		private const int MIN_TILE_TYPES = 3;

		#endregion

		#region PublicFields

		public int Width;
		public int Height;
		public float TileSize;
		public int Seed;

		[Header("Tiles")] public TilesConfig TilesConfig;
		public ETileType TileTypes;

		#endregion

		#region PublicMethods

		public List<TilesConfig.Tile> GetTilesConfig()
		{
			var tiles = new List<TilesConfig.Tile>();

			foreach (ETileType tileType in TileTypes.GetFlags())
			{
				TilesConfig.Tile tile = TilesConfig.Tiles.FirstOrDefault(t => t.TileType == tileType);

				if (tile.TileType == 0)
				{
					Debug.LogWarning($"Tiles config {TilesConfig} doesn't contain {tile}.");
					continue;
				}

				tiles.Add(tile);
			}

			if (tiles.Count < MIN_TILE_TYPES)
			{
				throw new Exception($"Level requires at least {MIN_TILE_TYPES} tiles type selected.");
			}

			return tiles;
		}

		public void GenerateSeed()
		{
			Seed = Guid.NewGuid().GetHashCode();
		}

		#endregion
	}
}
