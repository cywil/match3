using System;
using System.Collections.Generic;
using Match3.Enums;
using Match3.Views;
using UnityEngine;

namespace Match3.Scriptables
{
	[CreateAssetMenu(fileName = "TilesConfig", menuName = "Match3/TilesConfig", order = 1)]
	public sealed class TilesConfig : ScriptableObject
	{
		#region PublicFields

		public TileView TileViewPrefab;
		public List<Tile> Tiles;

		#endregion

		#region NestedTypes

		[Serializable]
		public struct Tile
		{
			public ETileType TileType;
			public Color Color;
		}

		#endregion
	}
}
