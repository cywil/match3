using System;
using System.Collections.Generic;

namespace Utils.Extensions
{
	public static class EnumExtensions
	{
		#region PublicMethods

		public static IEnumerable<T> GetFlags<T>(this T input)
			where T : Enum
		{
			foreach (T value in Enum.GetValues(input.GetType()))
			{
				if (input.HasFlag(value))
				{
					yield return value;
				}
			}
		}

		#endregion
	}
}
