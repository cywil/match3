using System;
using System.Runtime.CompilerServices;

namespace Utils.Extensions
{
	public static class ObjectExtensions
	{
		#region PublicMethods

		public static T CheckNotNull<T>(this T obj, [CallerMemberName] string context = "", string name = null)
			where T : class
		{
			if (obj == null)
			{
				throw new ArgumentException($"Value cannot be null! Context: {context}, name: {name ?? "<unknown>"}");
			}

			return obj;
		}

		#endregion
	}
}
