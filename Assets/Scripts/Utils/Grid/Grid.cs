using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.Grid
{
	public sealed class Grid<T> : IEnumerable<T>
		where T : class
	{
		#region PublicFields

		public int Width { get; }
		public int Height { get; }

		#endregion

		#region PrivateFields

		private T[,] m_grid;

		#endregion

		#region Constructors

		public Grid(int width, int height, Func<Vector2Int, Grid<T>, T> nodeFactory)
		{
			Height = height;
			Width = width;

			m_grid = new T[Width, Height];

			if (nodeFactory == null)
			{
				throw new ArgumentNullException("Node factory is null.");
			}

			for (int i = 0; i < Height; i++)
			{
				for (int j = 0; j < Width; j++)
				{
					m_grid[j, i] = nodeFactory.Invoke(new Vector2Int(j, i), this);
				}
			}
		}

		#endregion

		#region InterfaceImplementations

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < m_grid.GetLength(1); i++)
			{
				for (int j = 0; j < m_grid.GetLength(0); j++)
				{
					yield return m_grid[j, i];
				}
			}
		}

		#endregion

		#region PublicMethods

		public void ClearNodes()
		{
			for (int i = 0; i < Height; i++)
			{
				for (int j = 0; j < Width; j++)
				{
					m_grid[j, i] = null;
				}
			}
		}

		public List<(T, Vector2Int)> GetNeighbours(Vector2Int startPosition, bool checkDiagonal = false)
		{
			var neighbours = new List<(T, Vector2Int)>();

			for (int x = -1; x <= 1; x++)
			{
				for (int y = -1; y <= 1; y++)
				{
					if (x == 0 && y == 0)
					{
						continue;
					}

					if (!checkDiagonal && Math.Abs(x) == Mathf.Abs(y))
					{
						continue;
					}

					Vector2Int neighbourPosition = startPosition + new Vector2Int(x, y);

					if (IsOnGrid(neighbourPosition))
					{
						T neighbourNode = GetNode(neighbourPosition);

						if (neighbourNode != null)
						{
							neighbours.Add((neighbourNode, neighbourPosition));
						}
					}
				}
			}

			return neighbours;
		}

		public bool AreNeighbours(Vector2Int positionA, Vector2Int positionB, bool checkDiagonal = false)
		{
			if (checkDiagonal && (positionA - positionB).sqrMagnitude <= 2)
			{
				return true;
			}

			return (positionA - positionB).sqrMagnitude <= 1;
		}

		public List<(T, Vector2Int)> GetNeighboursInDirection(Vector2Int startPosition, Vector2Int raycast)
		{
			var neighbours = new List<(T, Vector2Int)>();

			int xDirection = raycast.x >= 0 ? 1 : -1;

			for (int i = 1; i <= Mathf.Abs(raycast.x); i++)
			{
				Vector2Int neighbourPosition = startPosition + new Vector2Int(i * xDirection, 0);

				if (IsOnGrid(neighbourPosition))
				{
					T neighbourNode = GetNode(neighbourPosition);

					if (neighbourNode != null)
					{
						neighbours.Add((neighbourNode, neighbourPosition));
					}
				}
			}

			int yDirection = raycast.y >= 0 ? 1 : -1;

			for (int i = 1; i <= Mathf.Abs(raycast.y); i++)
			{
				Vector2Int neighbourPosition = startPosition + new Vector2Int(0, i * yDirection);

				if (IsOnGrid(neighbourPosition))
				{
					T neighbourNode = GetNode(neighbourPosition);

					if (neighbourNode != null)
					{
						neighbours.Add((neighbourNode, neighbourPosition));
					}
				}
			}

			return neighbours;
		}

		public void SetNode(Vector2Int position, T node)
		{
			if (!IsOnGrid(position))
			{
				throw new ArgumentOutOfRangeException($"Postition {position} is out ouf grid.");
			}

			m_grid[position.x, position.y] = node;
		}

		public T GetNode(Vector2Int position)
		{
			if (!IsOnGrid(position))
			{
				throw new ArgumentOutOfRangeException($"Postition {position} is out ouf grid.");
			}

			return m_grid[position.x, position.y];
		}

		public T GetNode(int x, int y)
		{
			return GetNode(new Vector2Int(x, y));
		}

		#endregion

		#region PrivateMethods

		private bool IsOnGrid(Vector2Int position)
		{
			return position.x >= 0 && position.x < Width && position.y >= 0 && position.y < Height;
		}

		#endregion
	}
}
